import java.nio.FloatBuffer;

public class ObjModifier {
    public static void centerVertices(FloatBuffer buf) {
        // We often get 3D models which are not positioned at the origin of the coordinate system. Placing such models
        // on a 3D map is harder for users. One solution is to reposition such a model so that its center is at the
        // origin of the coordinate system. Your task is to implement the centering.
        //
        // FloatBuffer stores the vertices of a mesh in a continuous array of floats (see below)
        // [x0, y0, z0, x1, y1, z1, ..., xn, yn, zn]
        // This kind of layout is common in low-level 3D graphics APIs. 
        
        var dimension = 3;
        
        // Copy values from FloatBuffer to an array
        var a = new float[buf.capacity()];
        var n = a.length;
        buf.get(a);

        var sums = new float[dimension];
        var avgs = new float[dimension];

        // Compute sums of x, y, z
        for (var i = 0 ; i < n; i+=dimension) {
            for (var k = 0; k < dimension; k++) {
                sums[k] += a[i + k];
            }
        }

        // Compute average value for x, y, z 
        for (var i = 0; i < dimension; i++) {
            avgs[i] = sums[i]/(n/dimension);
        }

        // Subtract average value from each coordinate
        for (var i = 0 ; i < n; i+=3) {
            for (var k = 0; k < 3; k++) {
                a[i + k] -= avgs[k];
            }
        }

        // Update FloatBuffer with new coordinates
        for (var i = 0; i < n; i++) {
            buf.put(i, a[i]);
        }
    }
}
